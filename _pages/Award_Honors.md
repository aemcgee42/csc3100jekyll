---
layout: page
title: "Awards and Honors"
permalink: /awards/
---
<ul> 
    <li><h3>Motlow State Community College:</h3></li>
     <ul>
        <li>Graduated <em>summa cum laude</em></li> 
        <li>Dean’s List</li> 
        <li>Outstanding Student of Computer Science Departmental Award</li> 
        <li>Who’s Who Among College Students Award</li> 
    </ul>
    <br><br>
    <li><h3>Tennessee Technological University</h3></li>
    <ul>
        <li>Dean’s List</li> 
        <li>Residential Scholar Award</li> 
    </ul>

    
</ul>
