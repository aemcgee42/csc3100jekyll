---
layout: page
title: "Core Values"
permalink: /values/
---

<ul> 
        <li><h4>Achievement</h4></li>
        <ul>
            <li>I want my work to be meaningful and give me a sense of accomplishment.</li>
            <li>I want to be able to contribute to my team's project effectively.</li> 
        </ul>
        <li><h4>Collaboration</h4></li>
         <ul>
            <li>I want to be able to work with a friendly and cooperative team that understands the importance of work.</li>
            <li>I want to be able to work with a team that assists its team members in whatever work they are doing.</li> 
        </ul>
        <li><h4>Responsibility</h4></li>
         <ul>
            <li>I want to be able to have jobs that I have been entrusted to carry out.</li>
            <li>I want to be known for managing my responsibilities in a successful manner.</li> 
        </ul>
         <li><h4>Duty</h4></li>
         <ul>
            <li>I have a deep respect of those who have authority over me or over someone else.</li>
            <li>I enjoy following all rules and regulations given to me, and I will carry them out faithfully.</li> 
        </ul>
        
         <li><h4>Knowledge</h4></li>
         <ul>
            <li>I enjoy learning new things and new ways to do tasks.</li>
            <li>I am always looking to improve myself with the experiences I have in life.</li> 
        </ul>
        <li><h4>Community</h4></li>
         <ul>
            <li>I enjoy helping others in need anyway I can.</li>
            <li>I want to be able to make a difference in my community for the benefit of the community itself</li> 
        </ul>


</ul>
