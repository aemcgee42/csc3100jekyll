---
layout: page
title: "Skills, Interests, and Competencies"
permalink: /skills/
---


<ul> 
    <li><h3>Programming languages:</h3></li>
     <ul>
        <li>Java</li> 
        <li>C++</li> 
        <li>Windows Intel x86 Assembly</li> 
        <li>bash</li> 
        <li>MySql</li> 
        <li>Prolog</li> 
        <li>Ruby</li> 
        <li>HTML5</li>
        <li>Markdown</li> 
    </ul>
    <br><br>
    <li><h3>Software</h3></li>
    <ul>
        <li>Microsoft Office</li> 
        <li>Microsoft Visual Studio</li> 
        <li>Windows x86 Assembly</li> 
        <li>Adobe Photoshop</li> 
        <li>Adobe Illustrator</li> 
        <li>VirtualBox</li> 
        <li>Cloud9 IDE</li> 
        <li>InVision Prototyping</li>
    </ul>
    <br><br>
    <li><h3>Operating Systems</h3></li>
   <ul>
        <li>Windows (95, 98, 2000, XP, 7, 8, 10),</li> 
        <li>Mac OS</li> 
        <li>Linux (Debian)</li> 
    </ul>
     <br><br>
    <li><h3>Professional</h3></li>
   <ul>
        <li>Self-motivated</li> 
        <li>Creative Thinker</li> 
        <li>Detail-Oriented</li> 
        <li>Excellent Time Management Skills</li> 
        <li>Accept feedback and apply lessons learned</li> 
    </ul>
    
</ul>
