---
layout: page
title: "Work Experience"
permalink: /work_experience/
---

<ul> 
    <li><h3>Cookeville Regional Medical Center</h3></li>
    <h4>1 Medical Center Blvd, Cookeville, TN 38501, September 2016 – Current</h4>
     <h4><em>Project</em></h4>  
     <ul>
        &emsp;	<li>Responsibilities include developing a scheduling and productivity tool for the respiratory therapists</li> 
        &emsp;	<li>Learned various aspects of software engineering along with the agile method and how to work as a team</li> 
    </ul>
    <br><br>
    <li><h3>Granny Fishes’ House</h3></li>
    <h4>340 Shippman’s Creek Road, Wartrace, TN 37183, Jun 2013 - Current</h4>
    <h4><em>Dishwasher and Busboy</em></h4>  
    <ul>
        &emsp;	<li>Responsibilities include cleaning off tables, and cleaning all dishware and cookware.</li> 
        &emsp;	<li>Learned how to deal with stress, work with co-workers, and produce quality work.</li> 
    </ul>
    <br><br>
    <li><h3>Tullahoma Housing Authority</h3></li>
    <h4>1201 Cedar Lane, Tullahoma, TN 37388, Jun 2015 – August 2016</h4>
    <h4><em>Service Worker</em></h4>
    <ul>
        &emsp;	<li>Responsibilities include working with children and elderly and filling backpacks with school supplies to give to the children.</li> 
        &emsp;	<li>Learned how to work as an effective team, work with the public, and develop relations with others.</li> 
    </ul>
    <br><br>
    <li><h3>Vector Marketing</h3></li>
    <h4>610 West College Street, Murfreesboro, TN 37130, Jun 2014 – August 2014</h4>
     <h4><em>Sales Representative</em></h4>  
     <ul>
        &emsp;	<li>Responsibilities include scheduling appointments and presenting products to potential customers</li> 
        &emsp;	<li>Learned how to develop communication skills, perform quality presentations, and manage paperwork</li> 
    </ul>
</ul>
