---
layout: page
title: "Home"
permalink: /home/
---


<h3>Austin E. McGee</h3>
<h4>Undergradute Student</h4>
<h4>Department of Computer Science</h4>
<h4>Tennessee Technological University</h4>
<h4>931-434-1835</h4>
<h4>aemcgee42@students.tntech.edu</h4>
<h4>Cookeville, TN 38506</h4>

<br><br>
<h4>Objectives:</h4>

<ul> 
  <li><h5>Seeking to graduate with a B.S. in Computer Science.</h5></li>
  <li><h5>Seeking to obtain a position at a well-known company that has 
potential to expand and will provide me with many opportunities to improve
my knowledge base and understanding so that I can advance in my career. </h5></li>
</ul>