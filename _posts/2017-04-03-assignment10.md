---
layout: post
title:  "Assignment 10"
date:   2017-04-03 15:00:00 +0000
author: Austin E. McGee
categories: spring17 assignments
tags: assignments, inclass
---

For this assignment, we added styling to the input forms and tables. We styled them using bootstrap by using the forms and
button classes. We also added icons for the home and account links on the navbar. 

