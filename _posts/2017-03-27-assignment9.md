---
layout: post
title:  "Assignment 09"
date:   2017-03-27 15:00:00 +0000
author: Austin E. McGee
categories: spring17 assignments
tags: assignments, inclass
---

For this assignment, we practiced fixing SQL queries that were weak to SQL injection attacks.

